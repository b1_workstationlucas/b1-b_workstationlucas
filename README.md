# Maîtrise de poste - Day 1

## Self-footprinting

### Host OS

- Nom de la machine
```

PS C:\Users\hurlu\travail\debut\proj1> Get-ComputerInfo
[...]
CsName                                                  : LAPTOP-B2UST3IV
[...]
```
- OS et Version

```
PS C:\Users\hurlu\travail\debut\proj1> Get-ComputerInfo
[...]
OsName                                                  : Microsoft Windows 10 Famille
[...]
OsVersion                                               : 10.0.18362
[...]
```
- architecture processeur (32-bit, 64-bit, ARM, etc)

```
PS C:\Users\hurlu\travail\debut\proj1> Get-ComputerInfo
[...]
OsArchitecture                                          : 64 bits
[...]
```
- quantité RAM et modèle de la RAM

```
PS C:\Users\hurlu\travail\debut\proj1> Get-WmiObject win32_physicalmemory
[...]
Capacity             : 8589934592
[...]
```
je n'ai pas trouvé le modèle.

### Devices

- marque et modèle du processeur
```
PS C:\Users\hurlu\travail\debut\proj1> Get-ComputerInfo
[...]
CsNumberOfLogicalProcessors                             : 8
CsNumberOfProcessors                                    : 1
CsProcessors                                            : {Intel(R) Core(TM) i5-8300H CPU @ 2.30GHz}
[...]
```
i5-8300H signifie que c'est un processeur i5 de 8ème génération et le "H" signifie High Performance Graphics

- marque et modèle de la carte graphique et du touchpad

```
PS C:\Users\hurlu\travail\debut\proj1> Get-WmiObject win32_videoController
[...]
Name                         : NVIDIA GeForce GTX 1650
[...]
```
je n'ai pas trouvé les infos du trackpad

#### disque dur

- marque et modèle du disque dur 
```
PS C:\Users\hurlu\travail\debut\proj1> Get-Disk

Number Friendly Name                                                                                                                      Serial Number                    HealthStatus         OperationalStatus      Total Size Partition
                                                                                                                                                                                                                                  Style
------ -------------                                                                                                                      -------------                    ------------         -----------------      ---------- ----------
0      INTEL SSDPEKNW512G8                                                                                                                0000_0000_0100_0000_E4D2_5C03... Healthy              Online                  476.94 GB GPT

```
- différentes partitions

```
PS C:\Users\hurlu\travail\debut\proj1> Get-Partition


   DiskPath : \\?\scsi#disk&ven_nvme&prod_intel_ssdpeknw51#5&4f3ae75&0&000000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                                                                                   Size Type
---------------  ----------- ------                                                                                                   ---- ----
1                            1048576                                                                                                260 MB System
2                            273678336                                                                                               16 MB Reserved
3                C           290455552                                                                                           476.03 GB Basic
4                            511428263936                                                                                           650 MB Recovery
```
- système de fichier des partitions

```
PS C:\Users\hurlu\travail\debut\proj1> Get-Volume

DriveLetter FriendlyName FileSystemType DriveType HealthStatus OperationalStatus SizeRemaining      Size
----------- ------------ -------------- --------- ------------ ----------------- -------------      ----
C           OS           NTFS           Fixed     Healthy      OK                    277.17 GB 476.03 GB
            RECOVERY     NTFS           Fixed     Healthy      OK                    231.83 MB    650 MB
```
- fonction de chaque partition

partition system: contient les informations nécessaires au démarrage de windows 10

partition recovery: sert pour les options de récupération de windows


### Users

- liste des utilisateurs

```
PS C:\Users\hurlu> Get-LocalUser

Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
DefaultAccount     False   Compte utilisateur géré par le système.
hurlu              True
Invité             False   Compte d’utilisateur invité
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.
```

l'utilisateur qui est en full admin est "Administrateur"

### Processus

- liste des processus

```
PS C:\Windows\system32> get-process

Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    385      24    18600      31604       0,19   4484   2 ApplicationFrameHost
    291      13     3084       9096       0,38   5004   0 AsusLinkNear
     57       4      760       2932       0,08   4976   0 AsusLinkNearExt
    281      13     7812      11656       0,75   4984   0 AsusLinkRemote
    167       9     2032       6260       0,19   4088   0 AsusOptimization
    282      14     2860      14724       0,16  12272   2 AsusOptimizationStartupTask
    128      11     1636       7288       0,02  14020   2 AsusOSD
    437      19     9076      19284       2,75   4960   0 AsusSoftwareManager
    351      22    32456      33668       0,28   4384   2 AsusSoftwareManagerAgent
    395      13     4540      12244       0,36   4992   0 AsusSystemAnalysis
    186       9     3024       8140       0,05   4968   0 AsusSystemDiagnosis
    689      24    37828      47828      61,31   7752   0 aswidsagent
    188      12     9940      16884       0,28  10856   0 audiodg
   6801     110   178956     236092      76,33   2176   0 AvastSvc
    583      31    17760      35056       0,14   9836   2 AvastUI
   2052      47    26144      41932       1,38  10740   2 AvastUI
    590      30    12520      34108       0,17  14668   2 AvastUI
    326      18     5496      23636       0,14   1204   2 backgroundTaskHost
    298      29     8616      28508       0,17   2368   2 backgroundTaskHost
    295      19    15392      27064       0,13  10460   2 backgroundTaskHost
    251      14     7360      17524       0,14  12900   2 conhost
    836      25     2036       5584       4,30    768   0 csrss
    612      20     2456       5400       1,47   6932   2 csrss
    391      16     3708      14408       1,00  11608   2 ctfmon
    425      20     6492      16724       1,17   2352   0 dasHost
     80       5      884       3660       0,03   3964   0 dasHost
    451      23    12872      25408       1,14    476   2 Discord
    662      30    73648      74416       2,50   6016   2 Discord
    321      22    10120      18284       0,20   6916   2 Discord
    827     107   253672     216600      43,73   9928   2 Discord
    244      18     9100      13680       0,08  11780   2 Discord
    714      42    31804      63632       5,23  12952   2 Discord
    253      30     7704      14696       0,25   6372   2 dllhost
    193      16     3032       9064       0,05   7792   0 dllhost
    943      35    56208      89432      38,98    396   2 dwm
    686      89   200304     116388       4,05  11900   2 EpicGamesLauncher
    106       7     1596       5156       0,02   4576   0 esif_uf
   2835      94    69216     135788      13,00  11108   2 explorer
     38       6     1856       3452       0,92    416   0 fontdrvhost
     38       7     2104       5152       0,17  12368   2 fontdrvhost
     93       6     1088       4632       0,03   2392   0 gameinputsvc
     71       6     1168       4252       0,00   9824   2 gameinputsvc
    461      21     5768      18480       1,69   5312   0 GamingServices
    101       6     1116       4200       0,03   5240   0 GamingServicesNet
    174      11     5132       6992       1,14   5124   0 ICEsoundService64
      0       0       60          8                 0   0 Idle
    168       9     1980       7700       0,13   2808   0 igfxCUIService
    141       8     1432       6224       0,05   1708   0 IntelCpHDCPSvc
    126       7     1396       5864       0,06   1896   0 IntelCpHeciSvc
    136       8     1292       5488       0,03   5924   0 jhi_service
    580      27    14060      57436       0,95  12596   2 LockApp
   1578      29     8068      20600       6,83    420   0 lsass
      0       0      184      43724       4,45   2640   0 Memory Compression
    824      49    48872      42348       0,88  13992   2 Microsoft.Photos
    271      15     3476      11904       0,66   2072   0 NVDisplay.Container
    606      25    27180      31312       1,97  15324   2 NVDisplay.Container
    689      27    37016      52892       2,13   5012   0 OfficeClickToRun
    612      54    19952      30172       4,17   5140   0 OneApp.IGCC.WinService
    761      50    23900      59244       9,77   6408   2 OneDrive
    602      41    62916      76236       3,03   3244   2 powershell
    233      30    22004      17376       0,52   8048   0 PresentationFontCache
      0      18     8732      37032       4,00    120   0 Registry
    146       8     1628       6568       0,05   5372   0 RstMwService
    288      12     2288       8248       0,17   5364   0 RtkAudUService64
    284      13     2536       3428       0,13  11384   2 RtkAudUService64
   4208       8     2228       6732       1,52   5356   0 RtkBtManServ
    299      16     6432      22208       0,23    440   2 RuntimeBroker
    279      15     3176      20076       0,09   3140   2 RuntimeBroker
    345      19     4772      25596       1,75   4168   2 RuntimeBroker
    312      18     6260      25776       3,52   8316   2 RuntimeBroker
    685      30    11136      39268       1,59   8764   2 RuntimeBroker
    402      21     8232      30916       2,22  10336   2 RuntimeBroker
    330      17     4744      25600       0,27  10960   2 RuntimeBroker
    206      13     2864      14004       0,20  11084   2 RuntimeBroker
    410      19     9636      30752       0,44  13680   2 RuntimeBroker
    786      78    39888      50928      21,89   8972   0 SearchIndexer
   1759     131   168924     255436      10,31   3360   2 SearchUI
    429      16     4452      14300       0,36  10212   0 SecurityHealthService
    146      10     1852       8940       0,05   9936   2 SecurityHealthSystray
    848      15     6476      13308       8,11     84   0 services
    425      19     5932      11420       3,02  12004   2 SettingSyncHost
     89       6     3256       5744       0,19  11484   0 SgrmBroker
    580      19     7044      25756       3,72   1116   2 sihost
    398      23     8164      23420       0,13   8708   2 smartscreen
     53       3     1184       1140       0,20    564   0 smss
    612      26     8592      21436       0,67   4356   0 spoolsv
    620      30    27992      80208       2,59   8420   2 StartMenuExperienceHost
    913      78    42000      74448      11,27  11448   2 steam
    229      14     5760      11052       0,27   8952   0 SteamService
    519      47    56748      81648       1,13   2340   2 steamwebhelper
    518      42    72852     100660       2,53   4428   2 steamwebhelper
    726      33    19972      51464       0,94   4500   2 steamwebhelper
    249      17     7928      14284       0,02   5480   2 steamwebhelper
    543      29    42228      55652       0,39  10772   2 steamwebhelper
    500      28    25856      43636       0,11  12680   2 steamwebhelper
    480      30    11324      26260       0,22  14644   2 steamwebhelper
   1265      25    13436      30236      12,22    596   0 svchost
     86       5      968       3596       0,02    752   0 svchost
    317      16     4760      20128       5,55    992   0 svchost
   1453      19     9216      15848      16,47   1208   0 svchost
    294      10     2868       8428       3,09   1256   0 svchost
    155      29     5952       9320       1,19   1308   0 svchost
    292      16     4216      17404       0,27   1380   0 svchost
    269      19     2900      10640       0,17   1456   0 svchost
    160       9     1748       9964       0,14   1460   0 svchost
    180      13     1920       6880       0,09   1472   0 svchost
    253      13     2536      10156       0,39   1552   0 svchost
    164      10     2260      11140       0,19   1560   0 svchost
    251      10     2140       7900       0,06   1720   0 svchost
    421      14    14288      15892       0,83   1888   0 svchost
    255      13     3520      17844       0,19   1948   2 svchost
    221      10     2232       6932       1,64   2096   0 svchost
    170       7     1720       6220       0,31   2104   0 svchost
    242      15     3032      12516       0,05   2128   2 svchost
    187      12     2392      10360       1,17   2168   0 svchost
    169       9     1952       7300       0,03   2180   0 svchost
    223      10     8616      16520      10,47   2224   0 svchost
    231      13     2984      11352       0,33   2272   0 svchost
    224      11     2644      12552      17,52   2280   0 svchost
    173       9     1992       7116       0,09   2304   0 svchost
    221       7     1320       5300       0,20   2316   0 svchost
    407      19     6572      14872       4,83   2432   0 svchost
    390      16     4864      12136       1,78   2516   0 svchost
    277      14     3336       7880      12,91   2592   0 svchost
    281      12     2928       9748       1,28   2632   0 svchost
    178      12     1960       7940       0,16   2672   0 svchost
    223      12     2460       9888       0,22   2736   0 svchost
    184       9     1904       7112       0,55   2940   0 svchost
    156      11     1828       6728       0,31   2960   0 svchost
    583      13     3112       8516       3,39   3036   0 svchost
    228      12     3680       9804       0,41   3128   0 svchost
    184       9     1964       6924       1,28   3156   0 svchost
    140       9     1628       6096       0,06   3232   0 svchost
    240      13     2892      13624       0,17   3284   0 svchost
    232      15     2360       7148       0,97   3348   0 svchost
    272      16     3100       9004       0,59   3380   0 svchost
    575      16     9804      18752       9,39   3404   0 svchost
    380      14     3528      12684       1,38   3468   0 svchost
    428      18     6436      16056       9,88   3596   0 svchost
    145      12     1936       6068       0,45   3756   0 svchost
    381      16     3088       9272       1,00   3764   0 svchost
    349      17     5016      14936       0,63   3864   0 svchost
    512      22     6648      17420       1,91   3936   0 svchost
    436      32    12248      19992       6,11   4412   0 svchost
    184      10     2064       6916       0,11   4476   0 svchost
    155       7     1560       5372       0,19   4584   0 svchost
    201      13     3016      14756       0,23   4748   0 svchost
    125       8     1500       6284       0,05   4784   0 svchost
    488      40     7904      17188       3,22   5024   0 svchost
    531      24    17276      31660       6,56   5048   0 svchost
    370      21    16376      23764      16,45   5088   0 svchost
    263      13     2788       7232       0,33   5132   0 svchost
    508      21     5180      13256       0,75   5172   0 svchost
    141       9     1868       7912       0,08   5196   0 svchost
    135       9     1632       5528       0,06   5384   0 svchost
    408      20     4916      21384       0,55   5416   0 svchost
    128       7     1296       5136       0,03   5424   0 svchost
    366      15     3908      11808       1,14   5432   0 svchost
    207      12     2360       7940       0,23   5472   0 svchost
    215      12     3836       8804       0,14   5596   0 svchost
    109       7     1328       4716       0,06   5824   0 svchost
    376      24     3420      11268       0,17   5972   0 svchost
    162       9     1792       7008       0,03   6384   0 svchost
    150       9     1828       6240       0,08   6580   0 svchost
    107       5     1156       4400       0,06   7028   0 svchost
    237      13     2984      10680       2,52   7152   0 svchost
    364      13     3320      10588       0,58   7424   0 svchost
    112       8     1432       5664       0,05   8760   0 svchost
    451      27     5868      20540       0,89   9276   0 svchost
    450      18     7920      25288       0,61   9408   2 svchost
    222      13     3072      11832       0,20   9496   0 svchost
    170      11     2384       8892       0,56   9660   0 svchost
    431      19     4324      16980       2,75  12080   0 svchost
    144       8     2888       8284       0,06  13340   0 svchost
    194      11     2256      10540       0,83  14204   0 svchost
    114       8     1536       5924       0,03  14924   0 svchost
    156       9     1848       8312       0,56  14932   2 svchost
    482      22     6972      29256       1,63  15212   2 svchost
   4778       0      200       2040     760,28      4   0 System
    857      42    25940       7132       0,70   4220   2 SystemSettings
    271      35     7800      16728       0,28   3984   2 taskhostw
    530      32    23536      38064       1,86   4804   0 taskhostw
    447      26    52816      36116       0,28  14288   2 UnrealCEFSubProcess
    127       8     1768       6720       0,02   8516   0 unsecapp
    110       9     1940       8600       0,03   6392   2 UserOOBEBroker
    520      28   173312     145284      38,53   1216   2 wallpaper32
    163      11     1676       6944       0,06    968   0 wininit
    270      12     2800      10216       0,22   5456   2 winlogon
    988      66    50144      12896       1,05   9220   2 WinStore.App
    195      12     4684      12880       1,09   6852   0 WmiPrvSE
    190      12     7344      10616       1,70   8276   0 WmiPrvSE
    200      10     4536      10596       0,16   2324   0 wsc_proxy
    340      15     6352      17876       1,81   1040   0 WUDFHost
    708      47    26460      10664       0,97   9024   2 YourPhone
  ```
    
- description de 5 processus:

svchost : sert d'hote pour les fonctionalitées DLL
wininit : sert à l'initialisation de windows
WmiPrvSE : permet d'obtenir des informations sur le système
services : responsable de démarrer et d'arrêter les services du système
csrss : responsable de la communication entre les applications et le kernel

- Processus lancés par l'utilisateur en full admin

```
PS C:\Users\hurlu> TASKLIST /FI "USERNAME eq Administrateur"
Information : aucune tâche en service ne correspond aux critères spécifiés.
```
apparemment aucun processus n'a été lancé par l'utilisateur en full admin


### Network

- liste des cartes réseau

```
PS C:\Windows\system32> systeminfo
[...]
Carte(s) réseau:                            3 carte(s) réseau installée(s).
                                            [01]: Realtek 8821CE Wireless LAN 802.11ac PCI-E NIC
                                                  Nom de la connexion : Wi-Fi
                                                  État :                Support déconnecté
                                            [02]: Realtek PCIe GbE Family Controller
                                                  Nom de la connexion : Ethernet
                                                  DHCP activé :         Oui
                                                  Serveur DHCP :        192.168.1.1
                                                  Adresse(s) IP
                                                  [01]: 192.168.1.13
                                                  [02]: fe80::1c24:5100:56eb:1cae
                                                  [03]: 2a01:cb19:660:c00:8552:196:2d82:67be
                                                  [04]: 2a01:cb19:660:c00:1c24:5100:56eb:1cae
                                            [03]: Bluetooth Device (Personal Area Network)
                                                  Nom de la connexion : Connexion réseau Bluetooth
                                                  État :                Support déconnecté
[...]
```

explication des cartes:

Realtek 8821CE Wireless LAN : carte wifi
Realtek PCIe GbE Family : carte ethernet
Bluetooth Device (Personal Area Network) : carte Bluetooth

- liste des ports TCP et UDP en utilisation

```
PS C:\WINDOWS\system32> netstat -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    192.168.43.84:55091    52.114.74.222:https    ESTABLISHED
 [Teams.exe]
  TCP    192.168.43.84:55104    40.67.251.132:https    ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.43.84:55154    162.159.130.234:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.43.84:55160    52.113.199.44:https    ESTABLISHED
 [Teams.exe]
  TCP    192.168.43.84:55409    162.159.129.233:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.43.84:55415    52.114.75.53:https     ESTABLISHED
 [Teams.exe]
  TCP    192.168.43.84:55433    a-0001:https           ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.43.84:55434    52.97.173.18:https     ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.43.84:55435    40.78.128.150:https    ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.43.84:55437    a23-57-80-121:https    CLOSE_WAIT
 [SearchUI.exe]
  TCP    192.168.43.84:55438    131.253.33.254:https   ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.43.84:55439    13.107.136.254:https   ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.43.84:55440    204.79.197.222:https   ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.43.84:55441    52.113.195.132:https   ESTABLISHED
 [Teams.exe]
  TCP    192.168.43.84:55442    52.113.195.132:https   ESTABLISHED
 [Teams.exe]
 ```
 
 fonction de chaque programme:
 
 svchost.exe: exécute des DLL.

 Discord.exe: discord est un programme VoIP qui permet de discuter avec les autres utilisateurs.
 
 SearchUI.exe: signifie Search User Interface, c'est l'interface de recherche de cortana.
 
 Teams.exe: c'est un outil de visioconférence fait par microsoft
## Scripting
- script 1
le script 1 est nommé footprinting.ps1 et sa sortie est:
```
PS C:\Users\hurlu\cours\windows client\Tp 1> .\footprinting.ps1
--------------------
footprinting report:
--------------------
Nom de la machine :LAPTOP-4DFELSLI
L'ip est : 192.168.43.84
Le nom de L'OS est : Microsoft Windows 10 Famille
La version de l'OS est : 10.0.18362
Date et heure du dernier allumage : 11/02/2020 08:47:33
L'OS est a jour: False
RAM Utilisee : 5.68 Go / RAM Dispo : 2.17 Go
Espace libre : 406Go / Espace utilise : 71Go
Utilisateurs : Administrateur DefaultAccount hurlu Invité WDAGUtilityAccount
La moyenne du Ping vers 8.8.8.8 est: 32ms
```
 
 - script 2
le script 2 est nommé tp1_script2.ps1

les deux scripts sont dans le dépôt
## Gestion de softs

### intérêt d'un gestionnaire de paquets
Un gestionnaire de paquet sert à installer, désinstaller et mettre à jour des logiciels.
Par rapport au téléchargement sur internet  on est sur de télécharger le bon fichier.
Le téléchargement ne dépend plus de nous grâce au gestionnaire de paquet, le gestionnaire télécharge le bon fichier. Les seuls acteurs du téléchargement sont le gestionnaire et l'éditeur de logiciel.
étant donné que l'on est sûr de télécharger le bon fichier il n'y a plus de problèmes liés au fait de télécharger un mauvais fichier.
### Utilisation d'un gestionnaire de paquets
Avec chocolatey on a la commande choco list --local-only:
cependant nous n'avons pas les paquets installés sans chocolatey

```
PS C:\Users\hurlu> choco list --local-only
Chocolatey v0.10.15
chocolatey 0.10.15
chocolatey-core.extension 1.3.5.1
GoogleChrome 86.0.4240.198
3 packages installed.
```
choco list --detailed nous donne plus de détails

```
PS C:\Users\hurlu> choco list --local-only --detailed
Chocolatey v0.10.15
[...]
GoogleChrome 86.0.4240.198
 Title: Google Chrome | Published: 15/11/2020
 Number of Downloads: n/a | Downloads for this version: n/a
 Package url
 Chocolatey Package Source: https://github.com/chocolatey-community/chocolatey-coreteampackages/tree/master/automatic/googlechrome
 Tags: google chrome web internet browser admin
 Software Site: https://www.google.com/chrome/browser/
 Software License: https://www.google.it/intl/en/chrome/browser/privacy/eula_text.html
 Description: Chrome is a fast, simple, and secure web browser, built for the modern web.

  ### Notes

  * This package uses Chrome's administrative MSI installer and installs the 32-bit on 32-bit OSes and the 64-bit version on 64-bit OSes. If this package is installed on a 64-bit OS and the 32-bit version of Chrome is already installed, the package keeps installing/updating the 32-bit version of Chrome.
  * This package always installs the latest version of Google Chrome, regardless of the version specified in the package. Google does not officially offer older versions of Chrome for download. Because of this you may get checksum mismatch between the time Google releases a new installer, and the package is automatically updated.

3 packages installed.
```
la provenance des paquets se trouve dans "Chocolatey Package Source"

## Machine virtuelle

On a configuré VirtualBox, installé la VM et installé l'OS pendant le cours+-

### Configuration post-install

```
[root@localhost ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:63:36:29 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 85672sec preferred_lft 85672sec
    inet6 fe80::9d2f:bfcb:55ef:92d7/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:c9:0d:77 brd ff:ff:ff:ff:ff:ff
    inet 192.168.120.50/24 brd 192.168.120.255 scope global noprefixroute dynamic enp0s8
       valid_lft 1126sec preferred_lft 1126sec
    inet6 fe80::7a54:2a95:2066:988f/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
```
PS C:\Users\hurlu> ssh root@192.168.120.50
root@192.168.120.50's password:
```

Je suis sous windows donc pour faire un partage de fichier il me suffit de créer un document, aller dans propriétés, partage, partage avancé, autorisations puis autoriser la lecture, l'écriture et le "Contrôle total", j'applique les paramètres et le partage est fini du côté serveur.

on passe ensuite au côté client sur la VM
```
[root@localhost ~]# yum install -y cifs-utils
```
```
[root@localhost ~]# mkdir /opt/partage
```
```
[root@localhost ~]# mount -t cifs -o username=hurlu,password=MotDePasse //192.168.120.1/PartageTp1 /opt/partage
```
j'ai crée un fichier txt avec du texte dedans depuis windows pour le modifier depuis la VM

![](https://i.imgur.com/o1fv3U5.png)


```
[root@localhost ~]# cd ..
[root@localhost /]# cd opt/partage/
[root@localhost partage]# ls
coucou.txt
```
le fichier s'y trouve, il faut maintenant le modifier

```
[root@localhost partage]# nano coucou.txt
```
![](https://i.imgur.com/uy0wVuE.png)
![](https://i.imgur.com/I9j7Iwc.png)

une fois le fichier modifié on regarde si la modification a été prise en compte du côté windows

![](https://i.imgur.com/GhbmDNZ.png)

on regarde ensuite si l'on peut créer et supprimer un fichier depuis la VM

```
[root@localhost partage]# touch test
[root@localhost partage]# ls
coucou.txt  test
[root@localhost partage]# rm test
rm: remove regular empty file ‘test’? y
[root@localhost partage]# ls
coucou.txt
```

le partage est fonctionnel, on peut créer, éditer et supprimer des fichiers depuis la VM