# Lucas Fallous
# 19/10/2020
# affiche un résumé de L'OS, une liste des utilisateurs et calcule le temps de résponse vers 8.8.8.8
$rammax = [Math]::Truncate((Get-WmiObject Win32_ComputerSystem).TotalPhysicalMemory/1GB*100)/100
$ramlibre = [Math]::Truncate((Get-WmiObject Win32_OperatingSystem).FreePhysicalMemory/1MB*100)/100
$ramutilisee = ($rammax - $ramlibre)
$listemaj = Start-WUScan
If($null -ne $listemaj) {$ajour = $False} Else {$ajour = $True}
Write-Output "--------------------"
Write-Output "footprinting report: "
Write-Output "--------------------"
Write-Output "Nom de la machine :$([system.environment]::MachineName)"
Write-Output "L'ip est : $((Get-WmiObject -Class Win32_NetworkAdapterConfiguration | Where-Object {$_.DefaultIPGateway -ne $null}).IPAddress | select-object -first 1)"
Write-Output "Le nom de L'OS est : $((Get-WmiObject Win32_OperatingSystem).name.split("|")[0])"
Write-Output "La version de l'OS est : $((Get-WmiObject Win32_OperatingSystem).version)"
Write-Output "Date et heure du dernier allumage : $((Get-CimInstance Win32_OperatingSystem).LastBootUpTime)"
Write-Output "L'OS est a jour: $ajour"
Write-Output "RAM Utilisee : $ramutilisee Go / RAM Dispo : $ramlibre Go"
$free=0
$total=0
Get-WmiObject Win32_LogicalDisk | ForEach-Object -Process {$free=$free+$_.FreeSpace;$total=$total+$_.Size}
Write-Output "Espace libre : $([int][Math]::Ceiling($free/1GB))Go / Espace utilise : $([int][Math]::Ceiling(($total-$free)/1GB))Go"
Get-LocalUser |  ForEach-Object -Process {$Users=$Users+$_.Name+" "}
Write-Output "Utilisateurs : $Users"
$moyennePing = (ping 8.8.8.8).split("=")[-1]
Write-Output "La moyenne du Ping vers 8.8.8.8 est:$moyennePing"
