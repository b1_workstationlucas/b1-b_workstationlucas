# Lucas Fallous
# 19/10/2020
# lock ou shutdown le pc en fonction des arguments donnés
$action = $args[0]
$temps = $args[1]
if($action -eq "shutdown") {
    if($null -eq $temps){
        Stop-Computer -ComputerName localhost -Force
    } else {
        Start-Sleep $temps
        Stop-Computer -ComputerName localhost -Force
    }
} elseif ($action -eq "lock") {
    if($null -eq $temps){
        rundll32.exe user32.dll,LockWorkStation
    } else {
        Start-Sleep $temps
        rundll32.exe user32.dll,LockWorkStation
    }
}
